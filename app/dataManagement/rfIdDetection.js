/**
 * Created by Suhas on 9/9/2016.
 */
var rfIdModel = require('../models/rfIdDetection');
var q = require('q');
var getHistoricalData = function (startDate, interval, callback) {
    var aggregationStartDate = new Date(startDate);
    var aggregationEndDate = new Date(startDate);
    aggregationEndDate.setSeconds(aggregationEndDate.getSeconds() - interval)
    var promises = [];
    for (var i = 0; i < 60; i++) {
        if (i == 0) {
        } else {
            aggregationStartDate.setSeconds(aggregationStartDate.getSeconds() - interval)
            aggregationEndDate.setSeconds(aggregationEndDate.getSeconds() - interval)
        }
        var promise = taxiEntryAndExitDataOverPeriod(aggregationStartDate, aggregationEndDate).then(function (result) {
            var count = {
                entryCount: 0,
                exitCount: 0
            };
            if (result.length > 0) {
                count = {
                    entryCount: result[0].entryCount,
                    exitCount: result[0].exitCount
                };
            }
            return q(count);
        })
        promises.push(promise)
    }
    q.all(promises).then(function (resultData) {
        var countData = resultData.reverse();
        var taxiEntryExitObjArray = [[], []]
        for (var i = 0; i < countData.length; i++) {
            taxiEntryExitObjArray[0].push(countData[i].entryCount);
            taxiEntryExitObjArray[1].push(countData[i].exitCount)
            if (i == countData.length - 1) {
                callback(taxiEntryExitObjArray)
            }
        }
    })

}


function taxiEntryAndExitDataOverPeriod(startDate, endDate) {
    var deferred = q.defer();
    var aggregationStartDate1 = new Date(startDate)
    var aggregationEndDate1 = new Date(endDate)
    rfIdModel.aggregate(
        [
            {
                $match: {
                    "created_at": {"$gt": aggregationEndDate1, "$lt": aggregationStartDate1}
                }
            },
            {
                $group: {
                    "_id": null,
                    "entryCount": {"$sum": {"$cond": [{"$eq": ["$direction", "entry"]}, 1, 0]}},
                    "exitCount": {"$sum": {"$cond": [{"$eq": ["$direction", "exit"]}, 1, 0]}}
                }
            }

        ], function (err, result) {
            if (err) {
                console.log(err.stack)
            } else if (result && result.length > 0) {
                deferred.resolve(result);
            } else if (result && result.length == 0) {
                deferred.resolve(result);
            }

        });
    return deferred.promise;
}
module.exports = {
    getHistoricalData: getHistoricalData
}