/**
 * Created by Suhas on 9/11/2016.
 */
var obdDeviceGpsModel = require('../models/obdDeviceGpsModel');
var mongoose = require('mongoose');
var save = function (obj) {
    return new Promise(function (resolve, reject) {
        try {
            var gpsDataObj = new obdDeviceGpsModel;
            gpsDataObj.gpsData = obj;
            gpsDataObj.save(function (err, gpsDataSaved) {
                if (err) {
                    reject(err)
                } else {
                    resolve(gpsDataSaved)
                }
            })
        } catch (e) {
            reject(e)
        }
    })

}
var update = function (obj) {
    obdDeviceGpsModel.findOneAndUpdate({_id: obj._id}, obj, function (err, data) {
        if (err) {
            console.log(err.stack)
        } else {
            console.log('updated')
        }
    })
}

var getDataByRangeForACar = function (startDate, endDate, carId) {
    /*var carObjectId = mongoose.Types.ObjectId(carId)*/
    console.log("+++++++++++++++++++++++++" + startDate)
    console.log("+++++++++++++++++++++++++" + endDate)
    return new Promise(function (resolve, reject) {
        try {
            obdDeviceGpsModel.aggregate(
                // Pipeline
                [
                    // Stage 1
                    {
                        $match: {
                            "created_at": {
                                "$gt": startDate,
                                "$lt": endDate
                            },
                            "carLinked.assetData.assetName": carId
                        }
                    },

                    // Stage 2
                    {
                        $sort: {
                            "created_at": -1
                        }
                    },

                    // Stage 3
                    {
                        $project: {
                            "gpsData.lat": 1,
                            "gpsData.lng": 1,
                            "gpsData.imei": 1,
                            "gpsData.speed": 1,
                            "gpsData.acktimestamp": 1,
                            "gpsData.timestamp": 1,
                            "gpsData.time": 1,
                        }
                    },

                    // Stage 4
                    {
                        $project: {
                            "lat": "$gpsData.lat",
                            "lng": "$gpsData.lng",
                            "imei": "$gpsData.imei",
                            "speed": "$gpsData.speed",
                            "acktimestamp": "$gpsData.acktimestamp",
                            "timestamp": "$gpsData.timestamp",
                            "time": "$gpsData.time",
                            "_id": 0,

                        }
                    }
                ]).allowDiskUse(true)
                .exec(function (err, data) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(data)
                    }
                });
        } catch (e) {
            reject(e)
        }

    })

}
module.exports = {
    save: save,
    update: update,
    getDataByRangeForACar: getDataByRangeForACar

}