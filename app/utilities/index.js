/**
 * Created by Suhas on 2/13/2016.
 */
var arrayMethods = require('./arrayMethods');
timeOutModule = require('./timeOut.js')
module.exports = {
    arrayMethods: arrayMethods,
    timeOut: timeOutModule
}