/**
 * Created by Suhas on 2/5/2016.
 */
var socketClientDetails = require('./socketClientDetails.js'),
    setSocketClientDetails = require('./setSocketClientDetails.js');
module.exports = {
    socketClientDetails: socketClientDetails,
    setSocketClientDetails: setSocketClientDetails
}