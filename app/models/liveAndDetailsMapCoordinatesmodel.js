/**
 * Created by dhanalakshmi on 4/9/16.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var mapCenterPlotsSchema = new Schema({
    "mapDetailsSchemaModelConfiguration": {}
}, {collection: "mapCenterPlotsSchemaModel"})
module.exports = mongoose.model('mapCenterPlotsSchemaModel', mapCenterPlotsSchema);
