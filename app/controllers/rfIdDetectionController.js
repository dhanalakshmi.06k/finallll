/**
 * Created by Suhas on 9/9/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    dataManagement = require('../dataManagement'),
    config = require('../../config/config');


var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};
router.get('/smrt/rfIdDetection/historicalData/:intervals', function (req, res) {
    dataManagement.rfIdDetection
        .getHistoricalData(new Date(), parseInt(req.params.intervals), function (data) {
            res.send(data)
        });
})