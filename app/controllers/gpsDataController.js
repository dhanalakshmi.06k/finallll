/**
 * Created by Suhas on 9/11/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    routeConfig = require('../../config/assetServiceConfig'),
    request = require('request'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
    secret = 'this is the secrete password';
var gpsDataManagement = require('../dataManagement').gpsData;
var SensorsModel = mongoose.model('sensors');
var golfNovModel = mongoose.model('glofDataNov');
var passtaDecModel = mongoose.model('passtadec');
var moment = require('moment');
var trailModel = mongoose.model('tripDatamodelNew');
module.exports = function (app) {
    app.use('/', router);
};

router.get('/gpsData/:startTime/:endTime/:carId', function (req, res) {
    var startDate = new Date(parseInt(req.params.startTime))
    var endTime = new Date(parseInt(req.params.endTime))
    gpsDataManagement.getDataByRangeForACar(startDate, endTime, req.params.carId)
        .then(function (data) {
            console.log(data);
            res.send(data)
        })
})

/*router.get('/deviceDetails/:type', function (req, res) {
 console.log("**************!/api/deviceDetails/!******************")
 console.log(req.params.type)
 SensorsModel.find({"sensorData.sensorType":req.params.type}, function (err,result) {
 if (err) {
 console.log(err.stack)
 } else {
 SensorsModel.aggregate( [ {$match:{"_id" :result[0]._id}},
 { $unwind : "$sensorData.assetsLinked" }
 ], function (err,resultData) {
 if (err) {
 console.log(err.stack)
 }

 else{
 var assetMongodbArray=[]
 assetMongodbArray.push(result[0]._id)
 for(var j=0;j<resultData.length;j++){
 assetMongodbArray.push(resultData[j].sensorData.assetsLinked.ref)
 }
 SensorsModel.find({ _id: { $in: assetMongodbArray } }, function (err,carAssetDetails) {
 res.send(carAssetDetails)
 })

 }
 })



 }
 });
 })*/
router.get('/assetByMongoDbId/:id', function (req, res) {
    console.log("**************!/api/deviceDetails/!******************")
    console.log(req.params.id)
    SensorsModel.find({"_id": req.params.id}, function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {

            res.send(result)


        }
    });
})

router.get('/allAssets', function (req, res) {
    console.log("**************!/api/deviceDetails/!******************")
    console.log(req.params.id)
    SensorsModel.find({}, function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {

            res.send(result)


        }
    });
})

var dataTrackerInfoDetails = {
    timerId: '',
    timerInterval: 2
}
router.get('/getGpsHistoricData/:startTime/:endTime/:carId', function (req, res) {
    console.log("******************startTimestartTimestartTime")
    console.log(req.params.endTime)
    console.log((moment(parseInt(req.params.startTime)).format('YYYY-MM-DD H:mm:ss')))
    console.log(req.params.startTime)
    console.log(moment(parseInt(req.params.endTime)).format('YYYY-MM-DD H:mm:ss'))
    console.log(req.params.carId)
    console.log("******************startTimestartTimestartTime")
    trailModel.find(
        {
            $and: [{
                "device_data.created_at": {$gte: (moment(parseInt(req.params.startTime)).format('YYYY-MM-DD H:mm:ss'))}
            },
                {
                    "device_data.created_at": {$lte: (moment(parseInt(req.params.endTime)).format('YYYY-MM-DD H:mm:ss'))}
                },
                {
                    "device_data.imei": "OBD004"
                }]
        }
        , function (err, result) {
            if (err) {
                console.log(err.stack)
            } else {
                res.send(result)
            }
        }).sort({dt: -1})
})



router.get('/testdata', function (req, res) {
    var t = new Date("2016-11-30 23:37:30");

    console.log(moment(t).format('YYYY-MM-DD H:mm:ss'))
    console.log(moment(t.getTime() + (2 * 60 * 1000)).format('YYYY-MM-DD H:mm:ss'))
    /* getDeviceData(moment(t).format('YYYY-MM-DD H:mm:ss'),moment(t.getTime() + (2*60*1000)).format('YYYY-MM-DD H:mm:ss'),res)*/
    var endTime = t.getTime() + (2 * 60 * 1000)
    dataTrackerInfoDetails.timerId = setInterval(function () {
        getDeviceData(moment(endTime).format('YYYY-MM-DD H:mm:ss'),
            moment(endTime + (2 * 60 * 1000)).format('YYYY-MM-DD H:mm:ss'), res)


    }, dataTrackerInfoDetails.timerInterval * 1000);
})

function getDeviceData(startTime, endTime, res) {
    console.log("insideddd getDeviceData")
    console.log(startTime)
    console.log(endTime)


    golfNovModel.find({
        $and: [{dt: {$gte: moment(startTime).format('YYYY-MM-DD H:mm:ss')}}
            , {dt: {$lte: moment(endTime).format('YYYY-MM-DD H:mm:ss')}}]
    }, function (err, result) {
        if (err) {
            reject(new Error(err))
        }
        else {
            return result;
        }
    }).sort({dt: -1});
}

function invokeMsg(result, i) {
    pushToSocket(result[i]);
}

var deviceTrackerInfoDetails = {
    timerId: '',
    timerInterval: 10
}


router.get('/startTimerdata', function (req, res) {
    passtaDecModel.find({}, function (err, result) {
        var i = 1;
        if (err) {
            reject(new Error(err))
        }
        else {
            let thisDelay = 0;
            let interval;

            function startTimer() {
                console.log("start???")
                deviceTrackerInfoDetails.timerId = setTimeout(function () {
                    timeDiff = Math.abs(new Date(result[i--].dt).getTime() - new Date(result[i++].dt).getTime());
                    if (timeDiff < 15000) {
                        interval = 5;
                    }
                    else {
                        interval = 20;
                    }
                    thisDelay = timeDiff;
                    thisDelay = thisDelay
                    console.log(thisDelay)
                    pushToSocket(result[i++], thisDelay);
                    startTimer();
                }, thisDelay / interval);
            }

            startTimer();
        }
    }).sort({dt: -1});
})
router.get('/stopTimerdata', function (req, res) {
    console.log("stopTimerdata???")
    clearInterval(deviceTrackerInfoDetails.timerId);
})

var socketConn = require('../../config/socketIo');

function pushToSocket(data, delay) {
    var obj = {}
    obj.data = data;
    obj.delay = delay
    socketConn.getSocketIoServer().emit('carTrackerNotifier', obj);

}

router.get('/testHistoryTimerdata', function (req, res) {
    golfNovModel.find({}, function (err, result) {
        if (err)
            res.send(err)
        res.send(result)
    }).sort({dt: -1});


})