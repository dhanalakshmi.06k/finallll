/**
 * Created by zendynamix on 8/11/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    routeConfig = require('../../config/assetServiceConfig'),
    request = require('request'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
    secret = 'this is the secrete password';
routeConfig = require('../../config/assetServiceConfig');
var dataTrackerModule = require('../odbDeviceTracking').dataTracker;
var assetDataManagement = require('../dataManagement').assets;
var trailModel = mongoose.model('tripDatamodelNew');
var jsonTrip1Data = require('../json/trip1.json');
var jsonTrip2Data = require('../json/trip2.json');
var jsonTrip3Data = require('../json/trip3.json');
var jsonTrip4Data = require('../json/trip4.json');
var _ = require("lodash");

var _setInterval = require('setinterval-plus');
var obdDeviceGPSWOXModel = mongoose.model('gpswoxobdDeviceDetails');

var assetServiceDetails = {
    dataTracker: {
        start: function (deviceName) {
            dataTrackerModule.stopTracking();
            dataTrackerModule.startTrackingByOBDDevice(deviceName);
            return "Tracking Cars Details started Successfully";
        },
        stop: function () {
            dataTrackerModule.stopTracking();
            return "Stopped Successfully";
        },
        status: false
    }
}

/*assetServiceDetails.dataTracker.start();*/


router.get('/dataTracker/trackCars/:status/:deviceName', function (req, res) {
    console.log("***************/dataTracker/trackCars/*************")
    console.log(req.params.deviceName)
    var message = assetServiceDetails.dataTracker[req.params.status](req.params.deviceName);
    res.send(message)
})


router.get('/arrayData', function (req, res) {
    var trailDetail = new trailModel();
    /*for(key in jsonTripData) {
     var value = jsonTripData[key];

     trailDetail.createdAt = value.createdAt
     trailDetail.lat = value.latitude
     trailDetail.lng = value.longitude
     trailDetail.speed = value.speed
     trailDetail.save(function(err,result){


     })
     }*/
    res.send("savdddd@@")
})


var trip1Keys = null;
var mockGpsFeederId;
var trip2Keys = null;
var tripDetails ={
    IMEI123:{
        counter:0,
        mockData:jsonTrip1Data,
        key:_.keys(jsonTrip1Data)
    },
    OBD002:{
        counter:0,
        mockData:jsonTrip2Data,
        key:_.keys(jsonTrip2Data)
    },
    OBD003:{
        counter:0,
        mockData:jsonTrip3Data,
        key:_.keys(jsonTrip3Data)
    },
    OBD004:{
        counter:0,
        mockData:jsonTrip4Data,
        key:_.keys(jsonTrip4Data)
    }
}
var startSavingTrip2Data = function () {
    mockGpsFeederId = new _setInterval(function(){
        _.map(_.keys(tripDetails),function(data){
            saveTrip2ObdDevice(data)

        })
    }, 2000);
}
startSavingTrip2Data();


function saveTrip2ObdDevice(obdName) {
    if(tripDetails[obdName].key!=null && tripDetails[obdName].key.length-1 == tripDetails[obdName].counter){
        tripDetails[obdName].counter = 0;
    }else{
        tripDetails[obdName].counter= tripDetails[obdName].counter+1;
    }
    var count = tripDetails[obdName].counter;
    var keyValue = tripDetails[obdName].key[count]
    var gpsObject = tripDetails[obdName].mockData[keyValue];
    if(gpsObject!=null){
        var trailDetailModel = new trailModel();
        trailDetailModel.device_data.imei = obdName;
        trailDetailModel.lat = gpsObject.latitude
        trailDetailModel.lng = gpsObject.longitude
        trailDetailModel.speed = gpsObject.speed
        trailDetailModel.device_data.created_at = new Date().getTime()
        trailDetailModel.device_data.updated_at = new Date().getTime()
        trailDetailModel.save();
    }
}



module.exports = function (app) {
    app.use('/', router);
};

router.get('/getGpswoxDeviceDetails', function (req, res) {
    obdDeviceGPSWOXModel.find({}, function (err, result) {
        res.send(result);
    })
});

/*

 router.get('/getAllDevices', function (req, res) {
 /!*
 obdDeviceGPSWOXModel.find({},function(err,result){
 res.send(result);
 })*!/
 var url=routeConfig.assetsApiBaseUrl+'get_devices?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;
 sendResponse('get', url, res)
 });

 router.get('/getDeviceHistoryById/:fromDate/:toDate/:fromTime/:toTime/:deviceId', function (req, res) {
 var url=routeConfig.assetsApiBaseUrl+'get_history?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash
 +'&device_id='+req.params.deviceId+'&from_date='+req.params.fromDate+'&from_time='+req.params.toTime+'&to_date='+req.params.toDate+'&to_time='+req.params.fromTime+'&snap_to_road=false';
 sendResponse('get', url, res)
 });
 router.post('/addDevice', function (req, res) {
 var url=routeConfig.assetsApiBaseUrl+'add_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;
 sendPostResponse('post', url, res,req.body)
 });

 router.get('/deleteDevice/:deviceApiRefId', function (req, res) {
 var url=routeConfig.assetsApiBaseUrl+'destroy_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash+'&device_id='+req.params.deviceApiRefId;
 sendResponse('get', url, res)
 });
 */


function sendPostResponse(method, url, res, requestBody) {
    var options = {
        method: method,
        url: url,
        body: requestBody,
        json: true
    };
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
}

function sendResponse(method, url, res) {
    var options = {
        method: method,
        url: url
    };
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
}


/*dataTrackerModule.getAllDeviceData();*/


router.get('/assetsLinked/:rootType/:linkedAssetType', function (req, res) {
    assetDataManagement
        .getAssetsLinkedToAssetTypeProvided(req.params.rootType, req.params.linkedAssetType)
        .then(function (assetData) {
            res.send(assetData)
        })
})














