/**
 * Created by Suhas on 2/23/2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    random = require('mongoose-simple-random'),
    request = require("request"),
    busEntryExitModel = mongoose.model('busEntryExitInfo'),
    saveDataToDb = require('../dataAccessModule');
var zlib = require('zlib')
var config = require('../../config/config');


var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};


var busEntryExitDataSimulatorId = 0;
router.get('/smrt/busEntryExitData/startDataPush', function (req, res) {
    if (busEntryExitDataSimulatorId == 0) {
        triggerBusEntryExitDataPusher();
        res.send("started")
    } else {
        res.send("Push Already Started No Need To Start Again")
    }
})

router.get('/smrt/busEntryExitData/stopDataPush', function (req, res) {
    clearInterval(busEntryExitDataSimulatorId);
    busEntryExitDataSimulatorId = 0;
    res.send("Bus Entry Exit Data Pusher Stopped")
})

function triggerBusEntryExitDataPusher() {
    busEntryExitDataSimulatorId = setInterval(function () {
        busEntryExitDataPusher();
        console.log("Started Bus")
    }, 10000)
    console.log("started busEntryExit Data Pusher");
}
function busEntryExitDataPusher() {
    busEntryExitModel.findOneRandom(function (err, result) {
        if (err) {
            console.log(err)
        }
        if (result) {
            saveDataToDb.save.busEntryExitInfoDataHandler.saveData(result);
        } else {
            console.log("No Data In Database (BEE)");
        }
    })
}
