/**
 * Created by Suhas on 2/12/2016.
 */
var busEntryExitInfoDataHandler = require('./busEntryExitInfoDataHandler.js'),
    sensorDetectionInfoDataHandler = require('./sensorDetectionInfoDataHandler.js');
/*cameraImageInfoDataHandler = require('./cameraImageInfoDataHandler.js');*/

module.exports = {
    busEntryExitInfoDataHandler: busEntryExitInfoDataHandler,
    sensorDetectionInfoDataHandler: sensorDetectionInfoDataHandler/*,
    cameraImageInfoDataHandler:cameraImageInfoDataHandler*/

}