/**
 * Created by dhanalakshmi on 17/4/18.
 */
var mongoose = require('mongoose'),
    socketIo = require('../../config/socketIo').getSocketIoServer();
var pushData = function (data) {
    console.log("**************pushData******************")
    console.log(data)
    socketIo.emit('cameraDataAvailable', data);

}
module.exports = {
    pushCameraData: pushData
}