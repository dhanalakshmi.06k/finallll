/**
 * Created by dhanalakshmi on 4/9/16.
 */
taxiFleetManager.service('maintainceBayService', ['$http', function ($http) {
    var historyIntervalId, mapDetailsObject;
    var maintainceBayService = {

        getMapCenterPlots: function (deviceApiRefId) {
            return $http.get('/settings/getMapCenterCordinates');
        },

        setHistroyInterval: function (intervalId) {
            historyIntervalId = intervalId;
        },
        getHistroyInterval: function () {
            return historyIntervalId;
        },
        setMapCenterDeatilsObject: function (mapObject) {
            mapDetailsObject = mapObject
        },
        getMapCenterDeatilsObject: function () {
            return mapDetailsObject

        }
    }
    return maintainceBayService;

}]);
