/**
 * Created by zendynamix on 18/8/16.
 */

taxiFleetManager.service('assetApiUrlService', ['$http', function ($http) {
    var asset = {
        //get baseUrl lang and hash of gpswox
        getAssetsConfig: function () {
            return $http.get('/assetConfig');
        },
        //list all the devices
        getGpswoxDeviceDetails: function () {
            return $http.get('/getGpswoxDeviceDetails');
        },
        addDevice: function (deviceObj) {
            return $http.post('/addDevice', deviceObj);
        },
        deleteDevice: function (deviceApiRefId) {
            return $http.get('/deleteDevice/' + deviceApiRefId);
        },
        editDevice: function () {

        },
        //list all the devices
        getDevices: function () {
            return $http.get('/getAllDevices');
        }


    }
    return asset;

}]);
