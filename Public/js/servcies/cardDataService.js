/**
 * Created by zendynamix on 9/11/2016.
 */

taxiFleetManager.factory("carDataService", function ($http, $rootScope) {
    var cardDetails = {}
    socket.on('cardDetails', function (data) {
        setCardDetails(data);
    })
    function setCardDetails(data) {
        cardDetails = data;
        $rootScope.$emit('cardDetails', function (data) {

        })
    }

    var getCardDetails = function () {
        return cardDetails;
    }
    return {
        getCardDetails: getCardDetails
    }
})