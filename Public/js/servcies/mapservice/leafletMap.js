/**
 * Created by zendynamix on 20/8/16.
 */
taxiFleetManager.factory('leafLetMapService', function (assetApiService, $rootScope,maintainceBayService) {
    var googleMapService = {
        map: {}
    }
    var mapConfigObject=maintainceBayService.getMapCenterDeatilsObject();
    var mymap,
        dir,
        layers,
        BING_KEY = mapConfigObject.maptype.bingMap.key,
        markerGroup,
        markerhasTabel=[{
            key:"" ,
            value:""

        }]
    var IncialTrackingMarker;

    var polyline,  animatedHistoryMarker ;
    var animatedMarker;

    var marker;
    function initLeafLetMap(lat, lng, name) {
        mymap = L.map(name).setView([lat, lng], 10);
        /*types of map Aerial Road*/
        var tileLayer = new L.BingLayer(BING_KEY, {type: 'AerialWithLabels'});
        mymap.addLayer(tileLayer);


    }
    function addMarkers(lat, lng, markerId) {
        console.log("addMarkers"+markerId)
       /* var markerIcon = L.icon({
            iconUrl: '/../../directives/templates/square-xxl.jpg',
            iconSize: [20, 20]
        });
*/
        var firefoxIcon = L.icon({
            iconUrl: 'http://joshuafrazier.info/images/firefox.svg',
            iconSize: [38, 95], // size of the icon
        });

        // create marker object, pass custom icon as option, add to map
        var marker = L.marker([lat, lng], {icon: firefoxIcon}).addTo(mymap);
        mymap.setView(new L.LatLng(lat, lng),18);
        mymap.panTo(new L.LatLng(lat, lng));

     /*   marker = L.Marker.movingMarker([[lat, lng],[lat, lng]], 1,{icon: markerIcon}).addTo(mymap).on('click', onClick);*/
     /*   var markerObj={}
        markerObj.key=markerId;
        markerObj.value=marker;
        markerhasTabel.push(markerObj);
         marker._leaflet_id = markerId;
        */

    }





    function onClick(e) {
        var dataBox = $(".mapContainer .rightDetails");
        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });
        showDeviceData(this, this._leaflet_id)
    }

    function showDeviceData(element, deviceId) {


        $(".deviceIndicatorMain").removeClass("active");

        $(element).parent().addClass("active");

        var dataBox = $(".mapContainer .rightDetails");
        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });
        $rootScope.$emit('deviceDetails', {deviceId: deviceId});


    };

    function removeIncialMarker() {
        if(marker){
            console.log("**************markermarkermarker***************************")
            mymap.removeLayer(marker);
        }
        else{
            console.log("**************markermarkermarker*elseelseelseelse**************************")
        }
    }
    function drawPolyline(directionArray) {
        var directionObjArray=[]
        for(var j=0;j<directionArray.length;j++){
            var duumyArr=new Array();
            duumyArr.push(parseFloat(directionArray[j].lat))
            duumyArr.push(parseFloat(directionArray[j].lng))
            directionObjArray.push(duumyArr)

        }
        mymap.setView([directionArray[directionArray.length-1].lat, directionArray[directionArray.length-1].lng], 18)
        startRealTimeMarkerMoving(directionObjArray)
    }

    function startRealTimeMarkerMoving(points){
        if(animatedHistoryMarker){
            mymap.removeLayer(animatedHistoryMarker);
        }

        var line = L.polyline(points)
        var myIcon = L.icon({
            iconUrl: '/../../directives/templates/square-xxl.jpg',
            iconSize: [15, 15]
        });
        console.log("******************line.getLatLngs()line.getLatLngs()*****************")
        console.log(line.getLatLngs())

            animatedHistoryMarker = L.animatedMarker(line.getLatLngs(), {
            icon: myIcon,
            interval: 10000//milliseconds
        });
        animatedHistoryMarker.start();
        mymap.addLayer(animatedHistoryMarker);


    }


    function setCenterLocationOfMap(lat, lng, zoomLevel) {
        mymap.setView([lat, lng], zoomLevel)
    }

    function drawPolylineMapBasedOnInterval(directionArray) {
        var directionObjArray=[]
        for(var j=0;j<directionArray.length;j++){
            var tempArr=new Array();
            tempArr.push(parseFloat(directionArray[j].lat))
            tempArr.push(parseFloat(directionArray[j].lng))
            directionObjArray.push(tempArr)

        }
        mymap.setView([directionArray[0].lat, directionArray[0].lng], 15)
        plotDirection(directionObjArray)
    }
    function plotDirection(points) {


         polyline = L.polyline(points,
            {
                color: '#ef4b42',
                weight: 4,
                opacity: 0.9,
                smoothFactor: 4
            }
        ).addTo(mymap);

      /*  var label = new L.Label({
            offset: [-40,-7]
        });*/

        startMarkerMoving(points)

    }

    function deletePolyLine(){
        if(polyline){
            animatedHistoryMarker.stop();
            mymap.removeLayer(animatedHistoryMarker);
            mymap.removeLayer(polyline);

        }

    }
    function startMarkerMoving(points){

        var line = L.polyline(points)
        var myIcon = L.icon({
            iconUrl: '/../../directives/templates/square-xxl.jpg',
            iconSize: [15, 15]
        });

         animatedHistoryMarker = L.animatedMarker(line.getLatLngs(), {
            icon: myIcon,
            interval: 150
        });

        animatedHistoryMarker.start();
        mymap.addLayer(animatedHistoryMarker);

    }

    function instantiateMap(lat, lng, name,zoomLevel) {
         if (mymap != undefined) { mymap.remove(); }
        mymap = L.map(name).setView([lat, lng], zoomLevel);
        /*types of map Aerial Road*/
        var tileLayer = new L.BingLayer(BING_KEY, {type: 'AerialWithLabels'});
        mymap.addLayer(tileLayer);

    }



    function startMarkerMovingBetweenPoints(pointA,pointB,delay){
        console.log("***************pointArray**************")
        console.log(pointA)
        console.log(pointB)
        console.log("***************pointArray**************")

        var directionObjArray=[]
        var tempArr1=new Array();
        tempArr1.push(parseFloat(pointA.lat))
        tempArr1.push(parseFloat(pointA.lng))
        var tempArr2=new Array();
        tempArr2.push(parseFloat(pointB.lat))
        tempArr2.push(parseFloat(pointB.lng))
        directionObjArray.push(tempArr1)
        directionObjArray.push(tempArr2)
        var myIcon = L.icon({
            iconUrl: '/../../directives/templates/square-xxl.jpg',
            iconSize: [20, 20]
        });

        animatedHistoryMarker = L.animatedMarker(directionObjArray, {
            icon: myIcon,
            interval: delay//milliseconds
        });
        animatedHistoryMarker.start();
        mymap.addLayer(animatedHistoryMarker);
        mymap.setView([pointB.lat, pointB.lng], 25)
         mymap.panTo(new L.LatLng(pointB.lat, pointB.lng));


    }

    function plotDirectionArray(directionArray) {

        var directionObjArray=[]
        if(directionArray.length>1){
            for(var j=0;j<directionArray.length;j++){
                var duumyArr=new Array();
                duumyArr.push(parseFloat(directionArray[j].lat))
                duumyArr.push(parseFloat(directionArray[j].lng))
                directionObjArray.push(duumyArr)

            }
            mymap.setView([directionArray[directionArray.length-1].lat, directionArray[directionArray.length-1].lng], 18)
            startRealTimeMarkerMoving(directionObjArray)
        }
        else{
            mymap.setView([directionArray[0].lat, directionArray[0].lng], 18);
            var duumyArr=new Array();
            duumyArr.push(parseFloat(directionArray[0].lat))
            duumyArr.push(parseFloat(directionArray[0].lng))
            directionObjArray.push(duumyArr)
            startRealTimeMarkerMoving(directionObjArray);
        }

    }

    function startRealTimeMarkerMoving(points){


        if(animatedHistoryMarker){
            mymap.removeLayer(animatedHistoryMarker);
        }

        var line = L.polyline(points)
        var myIcon = L.icon({
            iconUrl: '/../../directives/templates/square-xxl.jpg',
            iconSize: [15, 15]
        });
        console.log("******************line.getLatLngs()line.getLatLngs()*****************")
        console.log(line.getLatLngs())

        animatedHistoryMarker = L.animatedMarker(line.getLatLngs(), {
            icon: myIcon,
            interval: 10000//milliseconds
        });
        animatedHistoryMarker.start();
        mymap.addLayer(animatedHistoryMarker);


    }




    return {
        initMap: initLeafLetMap,
        addMarkers: addMarkers,
        drawPolyline: drawPolyline,
        setCenterLocationOfMap: setCenterLocationOfMap,
       /* updateMarkers:updateMarkers,*/
       /* panAndZoomLocation:panAndZoomLocation,*/
        drawPolylineMapBasedOnInterval:drawPolylineMapBasedOnInterval,
        deletePolyLine:deletePolyLine,
        instantiateMap:instantiateMap,
        removeIncialMarker:removeIncialMarker,
        startMarkerMovingBetweenPoints:startMarkerMovingBetweenPoints,
        plotDirectionArray:plotDirectionArray

    };
});