/**
 * Created by zendynamix on 11-Sep-16.
 */
taxiFleetManager.factory("taxiTrailService", function ($http) {
    var historyFromTime, historyToTime;
    var historyFromDate, historyToDate;

    var getHistoryPlotsBasedOnRange = function (fromDate, toDate, carId, fromTime, toTime) {/*fromTime,toTime,*/
        var startDate = new Date(fromDate);
        startDate.setHours(parseInt(fromTime))
        var endDate = new Date(toDate);
        endDate.setHours(parseInt(toTime));
        console.log("startDate******************startDate")
        console.log(startDate)
        console.log(endDate)

        return $http.get('/getGpsHistoricData/' + startDate.getTime() + '/' + endDate.getTime()+'/'+carId);

    }
    var getAllTaxiLinkedToObdDevice = function () {
        return $http.get('/assetsLinked/obdDevice/car')
    }

    var setFromTime = function (fromTime) {
        historyFromTime = fromTime;

    }
    var getFromTime = function () {
        return historyFromTime;

    }

    var setToTime = function (toTime) {
        historyToTime = toTime;

    }
    var getToTime = function () {
        return historyToTime;

    }
    var setFromDate = function (fromDate) {
        historyFromDate = fromDate;

    }
    var getFromDate = function () {
        return historyFromDate;

    }

    var setToDate = function (toDate) {
        historyToDate = toDate;

    }
    var getToDate = function () {
        return historyToDate;

    }
    return {
        getHistoryPlotsBasedOnRange: getHistoryPlotsBasedOnRange,

        getAllTaxiLinkedToObdDevice: getAllTaxiLinkedToObdDevice,
        setFromTime: setFromTime,
        getFromTime: getFromTime,
        setToTime: setToTime,
        getToTime: getToTime,
        setFromDate: setFromDate,
        getFromDate: getFromDate,
        setToDate: setToDate,
        getToDate: getToDate


    }

})
