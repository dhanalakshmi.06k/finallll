/**
 * Created by zendynamix on 28-05-2016.
 */

taxiFleetManager.factory("sensorService", function ($http) {
    var asset = {

        getAllAssetsDetailsByType: function (type) {
            return $http.get('/api/sensorsData/' + type)
        },
        getAllCarAssetsDetails: function (type) {
            console.log("*********type********type********" + type)
            return $http.get('/deviceDetails/' + type)
        },
        getAllCarAssetsDetailsByMongoDbId: function (mongoId) {
            console.log("*********mongoId********mongoId********" + mongoId)
            return $http.get('/assetByMongoDbId/' + mongoId)
        },
        getAllAssets: function () {
            console.log("*********mongoId********mongoId********")
            return $http.get('/allAssets')
        }
    }
    return asset;

})
