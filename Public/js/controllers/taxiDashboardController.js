/**
 * Created by Flashbox on 8/14/2016.
 */

    taxiFleetManager.controller("taxiDashBoardController", function ($scope, authInterceptor,
                                                                     configService, $rootScope, assetApiService, leafLetMapService, carTrackPollerService
        , maintainceBayService,userService, settingsService, constantService, $timeout, $interval, carDataService, $state, $location) {
    $scope.count = {
        numberOfObdDevice: 0
    }
    $scope.taxiEntryExitDetails = {
        busEntry: []
    }
    $scope.deviceTrackingLoop;
    $scope.odbDetails;
    $scope.odbDevices;
    $scope.driverNames = ["ABDUL HAFEEZ SHAH ", "MOHAMMED KHALIL", "RASHEED KULANGARAVALAPPIL MUHAMMED "
        , "MOHAMMED SALEEM ", "ABRAR ALI ", "Aabidullah", "Aabzaari", "Aadil", "Aakil", "Aaidun", "Aafaaq"]
    // .......... map init ...........
    function getAllDeviceFromApi() {
        assetApiService.getMapPlotsFromApi().then(function (result) {
            $scope.odbDetails = result;
            $scope.count.numberOfObdDevice = result.length;
            if($scope.odbDetails!=null && $scope.odbDetails.length>0){
                $scope.startTracking($scope.odbDetails[0]);

                $scope.checkboxSelectionX = $scope.odbDetails[0].name;
            }
        }, function error(errResponse) {
            console.log(errResponse);
        });
    }

    $scope.startTracking = function (deviceDetails) {
        startTrackingADevice(deviceDetails.name);
        $scope.currentSelecion = deviceDetails.id;
        //stop the previous poller and start new one
    }


    function startTrackingADevice(deviceName) {
        carTrackPollerService.startDevice(deviceName).then(function () {
            var previousDataPoint = {}
            var currentDataPoint = {}
            var counter = 0
            if ($location.path() == "/app/dashboard") {
                socket.on('carTrackerNotifier', function (data) {
                    if(data!=null && data.length>0){

                        $scope.currentSpeed=data[0].speed*(18/5)

                        if(Math.round($scope.currentSpeed)>70){
                            var obj={}
                            obj.deviceName=deviceName
                            obj.currentSpeed=Math.round($scope.currentSpeed)
                            obj.notificationType="speedNotification"

                            userService.notifyADMINEMAIL(obj)
                                .then(function (obj) {
                                    console.log("*********$scope.currentSpeed****************")
                                    console.log(Math.round($scope.currentSpeed))

                                })
                            $rootScope.$broadcast('speedNotification',obj)
                        }
                        leafLetMapService.plotDirectionArray(data);
                    }
                });
            }
        });
    }

    //on click change map localtions
    $scope.setMapCenterTaxis = function (mapView) {
        if ($scope.deviceTrackingLoop) $interval.cancel($scope.deviceTrackingLoop);
        var mapDetailsConfigObject = maintainceBayService.getMapCenterDeatilsObject()
        leafLetMapService.instantiateMap(mapDetailsConfigObject.liveTab[mapView].mapCenterCoOrdinates.lat,
            mapDetailsConfigObject.liveTab[mapView].mapCenterCoOrdinates.lng,
            'dashBoardMap', mapDetailsConfigObject.liveTab[mapView].mapCenterCoOrdinates.zoomLevel);
        getAllDeviceFromApi();

    }
    $scope.getDBXTaxis = function () {
        if ($scope.deviceTrackingLoop) $interval.cancel($scope.deviceTrackingLoop);
        leafLetMapService.instantiateMap(25.2516641, 55.3688243, 'dashBoardMap', 14.5);


    }
    $scope.getDWCTaxis = function () {
        if ($scope.deviceTrackingLoop) $interval.cancel($scope.deviceTrackingLoop);
        leafLetMapService.instantiateMap(24.8945412, 55.1563713, 'dashBoardMap', 15);

    }
    $scope.getJebelAli = function () {
        if ($scope.deviceTrackingLoop) $interval.cancel($scope.deviceTrackingLoop);
        leafLetMapService.instantiateMap(24.974922, 55.013379, 'dashBoardMap', 12.5);


    }
    $scope.getExpo2022 = function () {
        if ($scope.deviceTrackingLoop) $interval.cancel($scope.deviceTrackingLoop);
        leafLetMapService.instantiateMap(24.968824, 55.041597, 'dashBoardMap', 11.5);


    }
    /*updating Card details*/
    $scope.cardDetails = {
        DXB: 0,
        DWC: 0,
        'Jebel Ali': 0,
        'Expo 2020': 0,
        'Depo': 0
    }
    $rootScope.$on('cardDetails', function (data, args) {
        updateCardDetails();
        $scope.$apply();
    });
    function updateCardDetails() {
        var cardDetailArray = carDataService.getCardDetails();
        for (var i = 0; i < cardDetailArray.length; i++) {
            var cardObj = cardDetailArray[i];
            $scope.cardDetails[cardObj.cardName] = cardObj.value;
        }

    }

    function initFunctions() {
        getAllDeviceFromApi()
        console.log("*************$location.path()****************************" + $location.path())

    }

    initFunctions();



    $scope.$on("$destroy", function() {
        carTrackPollerService.stopDevice();
    });
})
