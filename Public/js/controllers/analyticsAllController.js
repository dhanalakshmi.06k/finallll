/**
 * Created by MohammedSaleem on 17/02/16.
 */

taxiFleetManager.controller("analyticsAllController", function ($scope, $rootScope, $timeout) {

    var map;

    function initMap() {
        var mapBackground = '#1d212f';
        if ($("body").hasClass("goldPlatedTheme")) {
            mapBackground = "#ffffff";
        }
        else {

        }
        // Create a new StyledMapType object, passing it the array of styles,
        // as well as the name to be displayed on the map type control.
        var styledMap = new google.maps.StyledMapType($scope.mapStyles,
            {name: "Styled Map"});

        // var mapCenterConfigObj=settingService.getMapConfigObj();
        var mapLatLng = {lat: 25.277906, lng: 55.414753};
        var mapOptions = {
            center: mapLatLng,
            scrollwheel: false,
            zoom: 16,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            backgroundColor: mapBackground,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };

        map = new google.maps.Map(document.getElementById('overviewMap'), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        if (!$("body").hasClass("goldPlatedTheme")) {
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }
        else {
        }
    }


    //...... Adding devices List.......
    $scope.addMarkers = function (type, lat, lng, name, mapPanDisableFlag) {
        var boxText = '<div class="deviceIndicatorMain"><div class="deviceIndicator ' + type + '" id="' + name + '"></div></div>';
        var myOptions = {
            content: boxText,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-15, -15),
            zIndex: null,
            enableEventPropagation: true,
            closeBoxURL: "",
            position: new google.maps.LatLng(lat, lng),
            disableAutoPan: mapPanDisableFlag
        };

        var ib = new InfoBox(myOptions);
        ib.open(map, $scope.mapOptions);
    };

    $rootScope.$on('mapResize', function () {
        if (!$scope.showNotification) {
            $timeout(function () {
                google.maps.event.trigger(map, "resize");
            }, 1000)
        }
    });

    initMap();
});




