/**
 * Created by zendynamix on 8/11/2016.
 */

var routeConfig = {
    assetsApiBaseUrl: 'http://139.59.12.122/api/',
    lang: 'en',
    user_api_hash: '$2y$10$kEyaw..jrwhW6mCQHVGGiul4MKkMFcoWWm.9XRXLyzS228NzFRwL.',
    localAssetBaseUrl: '/sensorServiceApi'

}

module.exports = routeConfig
